﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Task16
{
    class Person
    {
        public string firstName;
        public string lastName;
        public double phoneNumber;

        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public double PhoneNumber { get => phoneNumber; set => phoneNumber = value; }
        public Person(string firstName, string lastName, double phoneNumber)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.PhoneNumber = phoneNumber;
        }
    }
}
