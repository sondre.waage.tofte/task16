﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Task16
{
    class ContactListEnum : IEnumerator
    {
        public List<Person> ncontactList;
        int place = -1;

        public ContactListEnum(List<Person> list)
        {
            ncontactList = list;
        }


        public bool MoveNext()
        {
            bool canmove = false;
            place++;
            if (place<ncontactList.Count)
            {
                canmove = true;
            }
            return canmove;
        }

        public void Reset()
        {
            place = -1;
        }

        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }
        public Person Current
        {
            get
            {
                try
                {
                    return ncontactList[place];
                }
                catch (IndexOutOfRangeException)
                {
                    throw new InvalidOperationException();
                }
            }
        }
    }
}

