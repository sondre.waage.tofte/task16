﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Task16
{
    class ContacList : IEnumerable
    {
        public List<Person> ncontactList;
        public ContacList(List<Person> cList)
        {
            ncontactList = new List<Person>();

            for (int i = 0; i < cList.Count; i++)
            {
                ncontactList[i] = cList[i];
            }
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }

        public ContactListEnum GetEnumerator()
        {
            return new ContactListEnum(ncontactList);
        }
    }
}
